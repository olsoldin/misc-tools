#########################################
#   A script to see if there are active #
#   admin panels on a web server.       #
#                                       #
#   Usage:                              #
#       python admin_page_scan.py *url* #
#########################################
import sys
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

GREEN = "\033[92m"
CYAN = "\033[36m"
RESET = "\033[0m"
CURSOR_UP_ONE = "\x1b[1A"
ERASE_LINE = "\x1b[2K"
ERASE_PREV = CURSOR_UP_ONE + ERASE_LINE


def search(url):
    f = open("pages.txt", "r")
    pages = 0

    if not url.startswith("http://") and not url.startswith("https://"):
        url = "http://"+url
    if not url.endswith("/"):
        url += "/"

    for page in f:
        link = url+page
        req = Request(link)
        # print every url we try, then change it to green if it's a hit
        # or replace it with the next one if it's a miss
        sys.stdout.write(link)
        try:
            urlopen(req)
            # print can handle colours
            # but it adds a blank line to the end for some reason
            print(ERASE_PREV + GREEN + link + RESET + CURSOR_UP_ONE)
            pages += 1
        except (HTTPError, URLError):
            # sys.stdout.write doesn't add a newline to the output
            # but it doesn't do colours
            sys.stdout.write(ERASE_PREV)
            continue
    print(CYAN)
    print("Done. %d pages found" % pages)
    print(RESET)

if len(sys.argv) < 2:
    url = input("Site Name:\n> ")
else:
    url = sys.argv[1]

search(url)
